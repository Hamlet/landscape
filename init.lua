--[[
    Landscape Redo - Turns dirt with grass' sides into full grass sides.
    Copyright (C) 2018  Hamlet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]


--
-- Configuration
--

local landscape_abm = minetest.settings:get_bool("landscape_abm")
local landscape_lbm = minetest.settings:get_bool("landscape_lbm")
local landscape_profiler = minetest.settings:get_bool("landscape_profiler")
local landscape_remover = minetest.settings:get_bool("landscape_remover")

if (landscape_abm == nil) then
	landscape_abm = true
end

if (landscape_lbm == nil) then
	landscape_lbm = true
end

if (landscape_profiler == nil) then
	landscape_profiler = false
end

if (landscape_remover == nil) then
	landscape_remover = false
end


--
-- General variables
--

local minetest_log_level = minetest.settings:get("debug_log_level")
local mod_load_message = "[Mod] Landscape Redo [v0.1.1] loaded."
local mod_path = minetest.get_modpath("landscape")

local time_of_day = 0
local timer = 0
local function_time_start = 0.0
local function_time_end = 0.0
local abm_grass_time_start = 0.0
local abm_grass_time_end = 0.0
local abm_dry_grass_time_start = 0.0
local abm_dry_grass_time_end = 0.0
local lbm_grass_time_start = 0.0
local lbm_grass_time_end = 0.0
local lbm_dry_grass_time_start = 0.0
local lbm_dry_grass_time_end = 0.0
local vm_time_start = 0.0
local vm_time_end = 0.0
local abm_times = {}
local lbm_times = {}
local vm_times = {}
local function_times = {}
local average_abm_time = 0.0
local average_lbm_time = 0.0
local average_vm_time = 0.0
local average_function_time = 0.0
local slowest_function = 0.0
local slowest_abm = 0.0
local slowest_lbm = 0.0
local slowest_vm = 0.0

local mushroom1 = "flowers:mushroom_brown"
local mushroom2 = "flowers:mushroom_red"


--
-- New nodes
--

minetest.register_node("landscape:dirt_with_grass", {
	description = "Dirt with Grass",
	tiles = {
		"default_grass.png",	"default_dirt.png",
		"default_grass.png",	"default_grass.png",
		"default_grass.png",	"default_grass.png"
	},
	groups = {crumbly = 3, soil = 1, not_in_creative = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

minetest.register_node("landscape:dirt_with_dry_grass", {
	description = "Dirt with Dry Grass",
	tiles = {
		"default_dry_grass.png",	"default_dirt.png",
		"default_dry_grass.png",	"default_dry_grass.png",
		"default_dry_grass.png",	"default_dry_grass.png"
	},
	groups = {crumbly = 3, soil = 1, not_in_creative = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
})

minetest.register_alias("landscape:full_grass_block",
	"landscape:dirt_with_grass")

minetest.register_alias("landscape:full_dry_grass_block",
	"landscape:dirt_with_dry_grass")


--
-- Functions
--


local average_value = function(table)
	local sum = 0.0

	for n = 1, #table do
		sum = (sum + table[n])
	end

	sum = (sum / #table)

	return sum

end


local neighbour_check = function(pos)

	if (landscape_profiler == true) then 
		function_time_start = os.clock()
	end

	local check_result = "failed"
	local node_is_flora = false
	local node_is_cliff = false

	local neighbour_above = {x = pos.x, y = (pos.y + 1), z = pos.z}
	local neighbour_below = {x = pos.x, y = (pos.y - 1), z = pos.z}
	local neighbour_north = {x = pos.x, y = pos.y, z = (pos.z + 1)}
	local neighbour_east = {x = (pos.x + 1), y = pos.y, z = pos.z}
	local neighbour_south = {x = pos.x, y = pos.y, z = (pos.z - 1)}
	local neighbour_west = {x = (pos.x - 1), y = pos.y, z = pos.z}
	local neighbour_below_north = {x = pos.x, y = (pos.y - 1), z = (pos.z + 1)}
	local neighbour_below_east = {x = (pos.x + 1), y = (pos.y - 1), z = pos.z}
	local neighbour_below_south = {x = pos.x, y = (pos.y - 1), z = (pos.z - 1)}
	local neighbour_below_west = {x = (pos.x - 1), y = (pos.y - 1), z = pos.z}

	local neighbour_above = minetest.get_node(neighbour_above)
	local neighbour_below = minetest.get_node(neighbour_below)
	local neighbour_north = minetest.get_node(neighbour_north)
	local neighbour_east = minetest.get_node(neighbour_east)
	local neighbour_south = minetest.get_node(neighbour_south)
	local neighbour_west = minetest.get_node(neighbour_west)
	local neighbour_below_north = minetest.get_node(neighbour_below_north)
	local neighbour_below_east = minetest.get_node(neighbour_below_east)
	local neighbour_below_south = minetest.get_node(neighbour_below_south)
	local neighbour_below_west = minetest.get_node(neighbour_below_west)

	local node_name_above = neighbour_above.name
	local node_name_below = neighbour_below.name
	local node_name_north = neighbour_north.name
	local node_name_east = neighbour_east.name
	local node_name_south = neighbour_south.name
	local node_name_west = neighbour_west.name
	local node_name_below_north = neighbour_below_north.name
	local node_name_below_east = neighbour_below_east.name
	local node_name_below_south = neighbour_below_south.name
	local node_name_below_west = neighbour_below_west.name

	local node_group_above1 = minetest.get_item_group(node_name_above, "flora")
	local node_group_above2 = minetest.get_item_group(node_name_above, "leaves")
	local node_group_north = minetest.get_item_group(node_name_north, "flora")
	local node_group_east = minetest.get_item_group(node_name_east, "flora")
	local node_group_south = minetest.get_item_group(node_name_south, "flora")
	local node_group_west = minetest.get_item_group(node_name_west, "flora")
	local node_group_below_north1 =
		minetest.get_item_group(node_name_below_north, "flora")
	local node_group_below_east1 =
		minetest.get_item_group(node_name_below_east, "flora")
	local node_group_below_south1 =
		minetest.get_item_group(node_name_below_south, "flora")
	local node_group_below_west1 =
		minetest.get_item_group(node_name_below_west, "flora")
	local node_group_below_north2 =
		minetest.get_item_group(node_name_below_north, "leaves")
	local node_group_below_east2 =
		minetest.get_item_group(node_name_below_east, "leaves")
	local node_group_below_south2 =
		minetest.get_item_group(node_name_below_south, "leaves")
	local node_group_below_west2 =
		minetest.get_item_group(node_name_below_west, "leaves")
	local node_group_below_north3 =
		minetest.get_item_group(node_name_below_north, "water")
	local node_group_below_east3 =
		minetest.get_item_group(node_name_below_east, "water")
	local node_group_below_south3 =
		minetest.get_item_group(node_name_below_south, "water")
	local node_group_below_west3 =
		minetest.get_item_group(node_name_below_west, "water")


	if (node_name_below == "air") then
		node_is_cliff = true
	end

	if (node_is_cliff == false) then
		if (node_name_below_north == "air") or (node_name_below_east == "air")
		or (node_name_below_south == "air")	or (node_name_below_west == "air")
		or (node_name_below_north == mushroom1)
		or (node_name_below_north == mushroom2)
		or (node_group_below_north1 == 1)
		or (node_group_below_north2 == 1)
		or (node_group_below_north3 == 3)
		or (node_name_below_east == mushroom1)
		or (node_name_below_east == mushroom2)
		or (node_group_below_east1 == 1)
		or (node_group_below_east2 == 1)
		or (node_group_below_east3 == 3)
		or (node_name_below_south == mushroom1)
		or (node_name_below_south == mushroom2)
		or (node_group_below_south1 == 1)
		or (node_group_below_south2 == 1)
		or (node_group_below_south3 == 3)
		or (node_name_below_west == mushroom1)
		or (node_name_below_west == mushroom2)
		or (node_group_below_west1 == 1)
		or (node_group_below_west1 == 2)
		or (node_group_below_west3 == 3)
		then
			node_is_cliff = true
		end
	end

	if (node_is_cliff == false) then
		if (node_name_above == "air") or (node_name_above == mushroom1) or 
			(node_name_above == mushroom2) or (node_group_above1 == 1) or 		
			(node_group_above2 == 1) then
			if (node_name_north == "air") or (node_name_north == mushroom1) or
				(node_name_north == mushroom2) or (node_group_north == 1) and
				(node_name_east ~= "air") or (node_group_east == 1) and
				(node_name_south ~= "air") or (node_group_south == 1) and
				(node_name_west ~= "air") or (node_group_west == 1) then

					check_result = "passed"

			elseif (node_name_east == "air") or (node_name_east == mushroom1) or
				(node_name_east == mushroom2) or (node_group_east == 1) and
				(node_name_south ~= "air") or (node_group_south == 1) and
				(node_name_west ~= "air") or (node_group_west == 1) and
				(node_name_north ~= "air") or (node_group_north == 1) then

					check_result = "passed"

			elseif (node_name_south == "air") or (node_name_south == mushroom1)
				or	(node_name_south == mushroom2) or (node_group_south == 1)
				and (node_name_west ~= "air") or (node_group_west == 1)
				and (node_name_north ~= "air") or (node_group_north == 1)
				and (node_name_east ~= "air") or (node_group_east == 1) then

					check_result = "passed"

			elseif (node_name_west == "air") or (node_name_west == mushroom1) or
				(node_name_west == mushroom2) or (node_group_west == 1) and
				(node_name_north ~= "air") or (node_group_north == 1) and
				(node_name_east ~= "air") or (node_group_east == 1) and
				(node_name_south ~= "air") or (node_group_south == 1) then

					check_result = "passed"

			elseif (node_name_north == "air") or (node_name_north == mushroom1)
				or (node_name_north == mushroom2) or (node_group_north == 1)
				and (node_name_east == "air") or (node_group_east == 1)
				or	(node_name_east == mushroom1) or (node_name_east == mushroom2) 
				and (node_name_south ~= "air") or (node_group_south == 1)
				or	(node_name_south == mushroom1)
				or (node_name_south == mushroom2)
				and (node_name_west ~= "air") or (node_group_west == 1)
				or	(node_name_west == mushroom1) or (node_name_west == mushroom2) 
				then

					check_result = "passed"

			elseif (node_name_east == "air") or (node_name_east == mushroom1)
				or	(node_name_east == mushroom2) or (node_group_east == 1)
				and (node_name_south == "air") or (node_group_south == 1)
				or	(node_name_south == mushroom1)
				or (node_name_south == mushroom2)
				and (node_name_west ~= "air") or (node_group_west == 1)
				or	(node_name_west == mushroom1)
				or (node_name_west == mushroom2)
				and (node_name_north ~= "air") or (node_group_north == 1)
				or	(node_name_north == mushroom1)
				or (node_name_north == mushroom2)
				then

					check_result = "passed"

			elseif (node_name_south == "air") or (node_name_south == mushroom1)
				or	(node_name_south == mushroom2) or (node_group_south == 1)
				and (node_name_west == "air") or (node_group_west == 1)
				or	(node_name_west == mushroom1) or (node_name_west == mushroom2) 
				and (node_name_north ~= "air") or (node_group_north == 1)
				or	(node_name_north == mushroom1)
				or (node_name_north == mushroom2)
				and (node_name_east ~= "air") or (node_group_east == 1)
				or	(node_name_east == mushroom1)
				or (node_name_east == mushroom2)
				then

					check_result = "passed"

			elseif (node_name_west == "air") or (node_name_west == mushroom1)
				or (node_name_west == mushroom2) or (node_group_west == 1)
				and (node_name_north == "air") or (node_group_north == 1)
				or	(node_name_north == mushroom1)
				or (node_name_north == mushroom2)
				and (node_name_east ~= "air") or (node_group_east == 1)
				or	(node_name_east == mushroom1) or (node_name_east == mushroom2) 
				and (node_name_south ~= "air") or (node_group_south == 1)
				or	(node_name_south == mushroom1)
				or (node_name_south == mushroom2)
				then

					check_result = "passed"
			end
		end
	end

	if (landscape_profiler == true) then
		function_time_end = (os.clock() - function_time_start) * 1000

		table.insert(function_times, function_time_end)

		if (function_time_end > slowest_function) then
			slowest_function = function_time_end
		end
	end

	return check_result

end


--
-- Loaded block modifiers
--

if (landscape_remover == false) then
	-- Add new nodes

	minetest.register_on_generated(function()

		local vm, emin, emax = minetest.get_mapgen_object"voxelmanip"

		if (emin.y < -127) then
			return
		end

		local data = vm:get_data()
		local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
		local grass = minetest.get_content_id"default:dirt_with_grass"
		local dry_grass = minetest.get_content_id"default:dirt_with_dry_grass"
		local landscape_grass =
			minetest.get_content_id"landscape:dirt_with_grass"
		local landscape_dry_grass =
			minetest.get_content_id"landscape:dirt_with_dry_grass"
		
		for i in area:iterp(emin, emax) do

			if (landscape_profiler == true) then
				vm_time_start = os.clock()
			end

			if (data[i] == grass) then
				local pos = area:position(i)
				local test = neighbour_check(area:position(i))

				if	(test == "passed") then
					data[i] = landscape_grass
				end

			elseif (data[i] == dry_grass) then
				local test = neighbour_check(area:position(i))

				if	(test == "passed") then
					data[i] = landscape_dry_grass
				end
			end
		end

		vm:set_data(data)
		vm:write_to_map()

		if (landscape_profiler == true) then
			vm_time_end = (os.clock() - vm_time_start) * 1000

			table.insert(vm_times, vm_time_end)

			if (vm_time_end > slowest_vm) then
				slowest_vm = vm_time_end
			end
		end

	end)

	if (landscape_abm == true) then

		minetest.register_abm({
			label = "landscape:grass_abm",
			nodenames = {"default:dirt_with_grass"},
			neighbors = {"air"},
			interval = 45,
			chance = 50,
			action = function(pos, node)

				if (pos.y < -29) then
					return
				end

				time_of_day = minetest.get_timeofday() * 24000

				if (time_of_day < 5500) or (time_of_day > 17500) then
					return
				end

				if (landscape_profiler == true) then
					abm_grass_time_start = os.clock()
				end

				local above = {x = pos.x, y = (pos.y + 1), z = pos.z}
				if ((minetest.get_node_light(above) or 0) < 13) then
					return
				end

				local test = neighbour_check(pos)
				if	(test == "passed") then
					minetest.set_node(pos, {name = "landscape:dirt_with_grass"})
				end

				if (landscape_profiler == true) then
					abm_grass_time_end = (os.clock() - abm_grass_time_start) * 1000

					table.insert(abm_times, abm_grass_time_end)

					if (abm_grass_time_end > slowest_abm) then
						slowest_abm = abm_grass_time_end
					end
				end

			end,
		})

		minetest.register_abm({
			label = "landscape:dry_grass_abm",
			nodenames = {"default:dirt_with_dry_grass"},
			neighbors = {"air"},
			interval = 45,
			chance = 50,
			action = function(pos, node)

				if (pos.y < -29) then
					return
				end

				time_of_day = minetest.get_timeofday() * 24000

				if (time_of_day < 5500) or (time_of_day > 17500) then
					return
				end

				if (landscape_profiler == true) then
					abm_dry_grass_time_start = os.clock()
				end

				local above = {x = pos.x, y = (pos.y + 1), z = pos.z}
				if ((minetest.get_node_light(above) or 0) < 13) then
					return
				end

				local test = neighbour_check(pos)
				if	(test == "passed") then
					minetest.set_node(pos, {name = "landscape:dirt_with_dry_grass"})
				end

				if (landscape_profiler == true) then
					abm_dry_grass_time_end = (os.clock() -
						abm_dry_grass_time_start) * 1000

					table.insert(abm_times, abm_dry_grass_time_end)

					if (abm_dry_grass_time_end > slowest_abm) then
						slowest_abm = abm_dry_grass_time_end
					end
				end
			end,
		})
	end


	if (landscape_lbm == true) then

		minetest.register_lbm({
			name = "landscape:grass_lbm",
			nodenames = {"default:dirt_with_grass"},
			run_at_every_load = false,
			action = function(pos, node)

				if (pos.y < -29) then
					return
				end

				if (landscape_profiler == true) then
					lbm_grass_time_start = os.clock()
				end

				local test = neighbour_check(pos)
				if	(test == "passed") then
					minetest.set_node(pos, {name = "landscape:dirt_with_grass"})
				end

				if (landscape_profiler == true) then
					lbm_grass_time_end = (os.clock() - lbm_grass_time_start) * 1000

					table.insert(lbm_times, lbm_grass_time_end)

					if (lbm_grass_time_end > slowest_lbm) then
						slowest_lbm = lbm_grass_time_end
					end
				end
			end,
		})

		minetest.register_lbm({
			name = "landscape:dry_grass_lbm",
			nodenames = {"default:dirt_with_dry_grass"},
			run_at_every_load = false,
			action = function(pos, node)

				if (pos.y < -29) then
					return
				end

				if (landscape_profiler == true) then
					lbm_dry_grass_time_start = os.clock()
				end

				local test = neighbour_check(pos)
				if	(test == "passed") then
					minetest.set_node(pos, {name = "landscape:dirt_with_dry_grass"})
				end

				if (landscape_profiler == true) then
					lbm_dry_grass_time_end = (os.clock() - lbm_dry_grass_time_start)
						* 1000

					table.insert(lbm_times, lbm_dry_grass_time_end)

					if (lbm_dry_grass_time_end > slowest_lbm) then
						slowest_lbm = lbm_grass_time_end
					end
				end
			end,
		})
	end

else
	-- Remove new nodes

	minetest.register_lbm({
		name = "landscape:grass_remover_lbm",
		nodenames = {"landscape:dirt_with_grass"},
		run_at_every_load = false,
		action = function(pos, node)

			if (pos.y < -29) then
				return
			end

			minetest.set_node(pos, {name = "default:dirt_with_grass"})
		end,
	})

	minetest.register_lbm({
		name = "landscape:dry_grass_remover_lbm",
		nodenames = {"landscape:dirt_with_dry_grass"},
		run_at_every_load = false,
		action = function(pos, node)

			if (pos.y < -29) then
				return
			end

			minetest.set_node(pos, {name = "default:dirt_with_dry_grass"})
		end,
	})
end


if (landscape_profiler == true) then
	minetest.register_globalstep(function(dtime)

		timer = (timer + dtime)

		if (timer >= 300) then
			if (#abm_times ~= 0) then
				average_abm_time = average_value(abm_times)
			end

			if (#lbm_times ~= 0) then
				average_lbm_time = average_value(lbm_times)
			end

			if (#vm_times ~= 0) then
				average_vm_time = average_value(vm_times)
			end

			if (#function_times ~= 0) then
				average_function_time = average_value(function_times)
			end

			print("\nLandscape Redo [v0.1.1] - TIMES:")
			print(string.format("Average ABM time: %.2fms",
				average_abm_time))
			print(string.format("Slowest ABM time: %.2fms",
				slowest_abm) .. "\n")
			print(string.format("Average LBM time: %.2fms",
				average_lbm_time))
			print(string.format("Slowest LBM time: %.2fms",
				slowest_lbm) .. "\n")
			print(string.format("Average VM time: %.2fms",
				average_vm_time))
			print(string.format("Slowest VM time: %.2fms",
				slowest_lbm) .. "\n")
			print(string.format("Average function time: %.2fms",
				average_function_time))
			print(string.format("Slowest function time: %.2fms",
				slowest_function) .. "\n")

			timer = 0
			abm_times = {}
			lbm_times = {}
			vm_times = {}
			function_times = {}
			average_abm_time = 0.0
			average_lbm_time = 0.0
			average_vm_time = 0.0
			average_function_time = 0.0
			slowest_function = 0.0
			slowest_abm = 0.0
			slowest_lbm = 0.0
			slowest_vm = 0.0
		end
	end)
end


--
-- Minetest engine debug logging
--

if (minetest_log_level == nil) or (minetest_log_level == "action") or
	(minetest_log_level == "info") or (minetest_log_level == "verbose") then

	minetest.log("action", mod_load_message)
end
